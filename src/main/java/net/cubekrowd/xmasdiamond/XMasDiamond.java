package net.cubekrowd.xmasdiamond;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class XMasDiamond extends JavaPlugin implements Listener {

    private List<String> players;
    private boolean isEnabled;
    private boolean isAuto;
    private String start;
    private String end;

    public void onEnable() {
        saveDefaultConfig();
        getServer().getPluginManager().registerEvents(this, this);
        reloadConfigCache();
        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			public void run() {
				if(isAuto) autoMode();
			}
		}, 20L, 20L); //Every second
    }

    public void reloadConfigCache() {
        players = this.getConfig().getStringList("users");
        if(this.getConfig().getString("enable").equals("auto")) {
        	isEnabled = false;
        	isAuto = true;
        } else {
        	isEnabled = (boolean) this.getConfig().getBoolean("enable");
        }
        start = "2579/" + this.getConfig().getString("start"); //year 2579 used as a placeholder
        end = "2579/" + this.getConfig().getString("end");
    }

    public void onDisable() {
        saveConfig();
    }
    
    public void autoMode() { //Check what isEnabled should be
    	LocalDateTime currentD = LocalDateTime.now().withYear(2579);
    	LocalDateTime startD = LocalDateTime.parse(start,
    	        DateTimeFormatter.ofPattern("y/d/M H:m"));
    	LocalDateTime endD = LocalDateTime.parse(end,
    	        DateTimeFormatter.ofPattern("y/d/M H:m"));
    	
    	boolean toggleEnable = false;
    	if (startD.isBefore(endD)) { //ends same year it starts
    		if(!currentD.isBefore(startD) && (currentD.isBefore(endD) || endD.isBefore(startD))) {
    			toggleEnable = true;
    		} else {
    			toggleEnable = false;
    		}
    	} else { //ends year after start
    		if(!currentD.isBefore(endD) && currentD.isBefore(startD)) {
    			toggleEnable = false;
    		} else {
    			toggleEnable = true;
    		}
    	}
    	
    	if(toggleEnable != isEnabled) {
    		isEnabled = toggleEnable;
    		String msg = "disabled";
    		if(toggleEnable) msg = "enabled";
    		getLogger().info("Automatically " + msg + " based on time: "
    				+ currentD.toString());
    	}
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (!label.equalsIgnoreCase("xmas")) {
            return true;
        }

        if (args.length == 0) {
            if (!isEnabled) {
                sender.sendMessage(ChatColor.RED + "Plugin is not enabled.");
                return true;
            }
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "You must be a player to execute this command!");
                sender.sendMessage(ChatColor.RED + "(maybe you're looking for /xmas enable?)");
                return true;
            }

            Player p = (Player) sender;
            if (players.contains(p.getUniqueId().toString())) {
                p.sendMessage(ChatColor.RED
                        + "You've already collected this year's diamond! Come back next year!");
                return true;
            }

            if (p.getInventory().firstEmpty() == -1) {
                p.sendMessage(ChatColor.RED
                        + "Please have an open inventory slot before executing this command!");
                return true;
            }

            ItemStack diamond = new ItemStack(Material.DIAMOND, 1);
            ItemMeta diaMeta = diamond.getItemMeta();
            diaMeta.setDisplayName(ChatColor.GREEN + "Merry Christmas " + ChatColor.RED
                    + "from CubeKrowd " + Calendar.getInstance().get(Calendar.YEAR) + "!");
            diamond.setItemMeta(diaMeta);
            p.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.DARK_AQUA + "Cube" + ChatColor.GOLD
                    + "Krowd" + ChatColor.DARK_GRAY + "] " + ChatColor.RED + "Merry "
                    + ChatColor.GREEN + "Christmas!");
            p.spawnParticle(Particle.VILLAGER_HAPPY, p.getLocation().getX(),
                    p.getLocation().getY() + 1, p.getLocation().getZ(), 20, .5, .5, .5);

            players.add(p.getUniqueId().toString());
            p.getInventory().addItem(diamond);
            this.getConfig().set("users", players);
            saveConfig();

        } else if (args.length >= 1) {

            if (!sender.hasPermission("xmas.admin")) {
                sender.sendMessage(ChatColor.RED + "You do not have access to that command.");
                return true;
            }

            String action = args[0].toLowerCase();
            boolean toEnable = false;

            if (action.equals("enable")) {
                toEnable = true;
            } else if (action.equals("disable")) {
                toEnable = false;
            } else if (action.equals("auto")) {
            	if(isAuto) {
            		sender.sendMessage(ChatColor.RED + "xmasdiamond already on auto mode.");
            		return true;
            	}
                isAuto = true;
                this.getConfig().set("enable", "auto");
                saveConfig();
                sender.sendMessage(ChatColor.GREEN + "xmasdiamond now on auto mode.");
                return true;
            } else if (action.equals("reload")) {
                reloadConfig();
                reloadConfigCache();
                sender.sendMessage(ChatColor.GREEN + "Successfully reloaded.");
                return true;
            } else {
                sender.sendMessage(ChatColor.RED + "/xmas enable|disable|auto|reload");
                return true;
            }

            if (isEnabled != toEnable || isAuto) {
                isEnabled = toEnable;
                isAuto = false;
                this.getConfig().set("enable", toEnable);
                saveConfig();
                sender.sendMessage(ChatColor.GREEN + "xmasdiamond now " + action + "d.");
            } else {
                sender.sendMessage(ChatColor.RED + "xmasdiamond already " + action + "d!");
            }
        }

        return true;
    }
}
